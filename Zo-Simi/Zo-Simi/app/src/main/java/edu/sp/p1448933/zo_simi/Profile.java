package edu.sp.p1448933.zo_simi;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

// ContextMenu ref: http://stackoverflow.com/questions/17207366/creating-a-menu-after-a-long-click-event-on-a-list-view


public class Profile extends BaseActivity {

    private Functionality func = new Functionality();
    private TextView authortxt, posttxt;
    // Declare variable
    private ListView listview;
    private int countEvent;

    private List<ParseObject> ob;
    private RowItem item;
    private List<RowItem> rowItems;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setItemSelected(0);
        setTitle(getResources().getString(R.string.title_profile));

        dbHelper = new DBHelper(this);

        rowItems = new ArrayList<RowItem>();
        listview = (ListView) findViewById(R.id.lvPostedEvent);
        authortxt = (TextView) findViewById(R.id.tvAuthorName);
        posttxt = (TextView) findViewById(R.id.tvTotalPost);

        authortxt.setText("Name: " + func.GetUserName());


        rowItems.clear();
        Cursor cursor = dbHelper.getPostedEvents(func.GetUserName());

        //Log.d("Like CursorCount", Integer.toString(cursor.getCount()));


        if(cursor.moveToFirst())
        {
            try
            {
                do {
                    item = new RowItem(func.decodeSampledBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_IMAGEFILE)), 0, 120, 120), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_OBJECT_ID)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_EVENT_NAME)),
                            cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_DESCRIPTION)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_RATING)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_PRICE)));
                    rowItems.add(item);
                    // Get number of events
                    countEvent = rowItems.size();
                    posttxt.setText("Total suggested event: " + Integer.toString(countEvent));
                    Log.d("Total Posted CountEvent", Integer.toString(rowItems.size()));
                } while (cursor.moveToNext());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


        }

        CustomListViewAdapter adapter = new CustomListViewAdapter(Profile.this, R.layout.listview_item, rowItems);
        adapter.notifyDataSetChanged();

        // Binds the Adapter to the ListView
        listview.setAdapter(adapter);

        cursor.close();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Debugging purpose
                //func.showShortToast(getApplicationContext(), "YOU CLICKED ON ME : " + position
                //      + " rowitem pos: " + rowItems.get(position).getObjectId());

                // Send single item click data to SingleItemView Class
                if(func.CheckConnectivity(getApplicationContext()) == true)
                {
                    Intent i = new Intent(Profile.this,
                            BrowseEventDetail.class);
                    // Pass data "name" followed by the position
                    // Place the data here ("variable name" from intent) and getString("variable name") from Parse column name
                    i.putExtra("objectId", rowItems.get(position).getObjectId());
                    Log.d("Profile", "Profile Screen Obj Id: " + rowItems.get(position).getObjectId());
                    // Open BrowseEventDetail.java Activity
                    startActivity(i);
                }
                else
                {
                    func.showShortToast(getApplicationContext(), "Please turn on your Wi-Fi or Data");
                }

            }
        }); // End setOnItemClickListener()

        registerForContextMenu(listview);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId()==R.id.lvPostedEvent)
        {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }   // End onCreateContextMenu()

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId())
        {
            case R.id.edit:
                //Edit stuff here
                // To get ID
                final String objectId = rowItems.get(info.position).getObjectId();
                // Run this xml view when user click on edit
                setContentView(R.layout.profile_edit_eventname);
                // Delcare these element
                final EditText saveEventName = (EditText) findViewById(R.id.etPostedEventName);
                Button saveButton = (Button) findViewById(R.id.btnSave);
                saveEventName.setText(rowItems.get(info.position).getEventTitle());
                //saveEventName.setHint(rowItems.get(info.position).getEventTitle());
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(dbHelper.updateEventName(objectId, saveEventName.getText().toString()) == true)
                        {
                            func.showShortToast(getApplicationContext(), "Updated successfully");
                            func.GoToProfile(getApplicationContext());
                            // Parse Saving
                            ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                            query.getInBackground(objectId, new GetCallback<ParseObject>() {
                                @Override
                                public void done(ParseObject object, ParseException e) {
                                    if(e == null)
                                    {
                                        object.put("eventname", saveEventName.getText().toString());
                                        object.saveEventually();
                                    }
                                }
                            }); // End Parse Save
                        }
                        else
                        {
                            func.showShortToast(getApplicationContext(), "Update Failed");
                            func.GoToProfile(getApplicationContext());
                        }

                    }
                });
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void gettingUserInfo()
    {
        // Locate the listview in listview_main.xml
        listview = (ListView) findViewById(R.id.lvPostedEvent);
        // Retrieve object "name" from Parse.com database
        rowItems.clear();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                "Event");
        query.whereEqualTo("author", func.GetUserName());
        query.whereExists("ImageFile");
        query.orderByDescending("createdAt");
        try {
            ob = query.find();

            for(ParseObject event : ob)
            {
                //Log.e("Profile populateInfo", event.getObjectId() + " " + event.getString("eventname") + " " + event.getString("description") + " " +
                  //      event.getString("rating") + " " + event.getString("price") );
                // Pushing information into another method
                populateInfo(event.getObjectId(), event.getString("eventname"),
                        event.getString("description"), event.getString("rating"), event.getString("price"));

            }

        } catch (ParseException e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    } // End of gettingUserInfo()

    private void populateInfo(final String objectIdString,final String eventName, final String description,
                              final String rating,final String price) {
        // Debugging purpose to check data passed in
        // Log.e("Profile populateInfo", objectIdString + " " + eventName + " " + description + " " +
        //  rating + " " + price );


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.orderByDescending("createdAt");
        query.getInBackground(objectIdString, new GetCallback<ParseObject>() {

            @Override
            public void done(final ParseObject object, ParseException e) {
                if (e == null) {
                    // OK
                    ParseFile image = object.getParseFile("ImageFile");
                    {
                        image.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                item = new RowItem(func.decodeSampledBitmapFromByte(data, 0, 120, 120), objectIdString, eventName,
                                        description, rating, price);

                                rowItems.add(item);
                                // Get number of events
                                countEvent = rowItems.size();
                                posttxt.setText("Total suggested event: " + Integer.toString(countEvent));
                                // Debug purpose
                                //Log.d("BrowseEvent COUNT ITEM", Integer.toString(countEvent));

                                if (countEvent == 0) {
                                    // If no event retrieved
                                    func.showShortToast(getApplicationContext(), "Oops! sorry no event yet");
                                    //Log.e("BrowseEvent No Event", Integer.toString(countEvent));
                                } else {
                                    // Debugging purpose
                                    //Log.e("BrowseEvent Have Event", "Number of event: " + Integer.toString(countEvent));
                                }

                                CustomListViewAdapter adapter = new CustomListViewAdapter(Profile.this, R.layout.listview_item, rowItems);

                                //Notifies the attached observers that the underlying data has been
                                // changed and any View reflecting the data set should refresh itself.
                                adapter.notifyDataSetChanged();

                                // Binds the Adapter to the ListView
                                listview.setAdapter(adapter);


                                // Capture button clicks on ListView items
                                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        // Send single item click data to SingleItemView Class
                                        Intent i = new Intent(Profile.this,
                                                BrowseEventDetail.class);
                                        // Pass data "name" followed by the position
                                        // Place the data here ("variable name" from intent) and getString("variable name") from Parse column name
                                        i.putExtra("objectId", rowItems.get(position).getObjectId());
                                        // Open BrowseEventDetail.java Activity
                                        startActivity(i);
                                    }
                                }); // end setOnItemClickListener
                            }
                        });
                    }
                } else {
                    // PROBLEM fetching event info
                    e.printStackTrace();
                }


            }

/*
        adapter = new ArrayAdapter<String>(Profile.this,
                R.layout.listview_item);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.whereEqualTo("author", func.GetUserName());
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> results, ParseException e) {
                try {
                    for (ParseObject item : results) {
                        adapter.add(item.get("eventname").toString());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                countEvent = adapter.getCount();
                posttxt.setText("Post " + countEvent);

                adapter.notifyDataSetChanged();
                // Binds the Adapter to the ListView
                listview.setAdapter(adapter);
            } // End of done()
        }); // End of findInBackground()

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                func.GoToBrowseEvent(getApplicationContext());
            }
        });

    } // End of onCreate()*/

        });
    }
}
