package edu.sp.p1448933.zo_simi;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.like.IconType;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

// Retrieving Image from Parse : http://findnerd.com/list/view/Retrieve-Image-file-from-parse-com-and-set-on-ImageView-android/9356/
// Bookmark Icon Plugin : https://github.com/jd-alexander/LikeButton
// Fixing 'Out of memory' due to bitmap: http://stackoverflow.com/questions/7737415/out-of-memory-error-with-images
// Reference for building Google Map: http://www.androidhive.info/2013/08/android-working-with-google-maps-v2/

public class BrowseEventDetail extends BaseActivity {

    private TextView eventTitle, eventDescription, eventPrice, eventAuthor, eventCategory, eventPostDate;
    private RatingBar eventdetailRating;
    private String objectId;
    private ImageView eventImage;
    private Functionality func = new Functionality();
    private Bookmark bookmark;
    private com.like.LikeButton likeButton;

    private GoogleMap googleMap;
    private FrameLayout fl;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_event_detail);

        setTitle(getResources().getString(R.string.title_browseDetailEvent));
        ParseObject.registerSubclass(Bookmark.class);
        bookmark = new Bookmark();

        eventTitle = (TextView)findViewById(R.id.tvEventTitle);
        eventDescription = (TextView)findViewById(R.id.tvEventDescription);
        eventdetailRating = (RatingBar)findViewById(R.id.eventDetailRating);
        eventPrice = (TextView)findViewById(R.id.tvEventPrice);
        eventAuthor = (TextView)findViewById(R.id.tvEventAuthor);
        eventCategory = (TextView)findViewById(R.id.tvEventCategory);
        eventPostDate = (TextView)findViewById(R.id.tvEventPostDate);
        eventImage = (ImageView) findViewById(R.id.ivEventImage);
        likeButton = (com.like.LikeButton) findViewById(R.id.like_button);
        // Set the likebutton to thumb icon
        likeButton.setIcon(IconType.Thumb);
        eventdetailRating.setFocusable(false);
        // Use this to control map hide or show, depending if there's coord
        // By default hide it until there is any than show
        fl =  (FrameLayout) findViewById(R.id.mapFrame);
        fl.setVisibility(View.GONE);

        Bundle extra = getIntent().getExtras();
        if(extra != null)
        {
            // Collect info from previous intent (BrowseEvent.class)
            objectId = extra.getString("objectId");
            // Debugging purpose
            //Log.d("ObjectID", objectId);

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
            query.getInBackground(objectId, new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if(e == null) {
                        // object will be my Event

                        eventTitle.setText(object.getString("eventname"));
                        eventDescription.setText(object.getString("description"));
                        eventdetailRating.setRating(Float.parseFloat(object.getString("rating")));
                        if(object.getString("price").startsWith("0") == false)
                        {
                            eventPrice.setText("Estimated $" + object.getString("price"));
                        }
                        else
                        {
                            eventPostDate.setText("Pricing not available");
                        }
                        eventAuthor.setText(object.getString("author"));
                        eventCategory.setText(object.getString("category"));
                        eventPostDate.setText(func.getDate(object.getCreatedAt()));
                        if(func.CheckConnectivity(getApplicationContext()) == true && object.getString("latitude").startsWith("0.0") == false
                                && object.getString("latitude").isEmpty() == false)
                        {

                            try {
                                // Loading map
                                fl.setVisibility(View.VISIBLE);
                                initilizeMap(Double.parseDouble(object.getString("latitude")),Double.parseDouble(object.getString("longitude")), object.getString("locationname"));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }



                        ParseFile image = object.getParseFile("ImageFile");
                        // If there's image
                        if(image != null)
                        {
                            image.getDataInBackground(new GetDataCallback() {
                                @Override
                                public void done(byte[] data, ParseException e) {
                                    eventImage.setImageBitmap(func.decodeSampledBitmapFromByte(data, 0, 300, 300));
                                }
                            });
                        }

                    }
                    else
                    {
                        // something went wrong
                        Log.d("EventDetailed Error", e.getMessage());
                    }
                }
            }); // End of query.getDataInBackground()
        }
        else
        {
            func.showShortToast(getApplicationContext(), "Sorry, there's a problem.");
        }

        // Run this to check if event has already bookmarked by this user
        // Set toggled checked if already marked
        queryBookmark();

        // LikeButton actionlistener where checking for toggling
        likeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

                setBookMark();
                func.showShortToast(getApplicationContext(), "Liked");
                delaySqlCheck(3000);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                deleteBookmark();
                func.showShortToast(getApplicationContext(), "Unliked");
                delaySqlCheck(3000);

            }
        });
    }   // End of onCreate()

    private void setBookMark()
    {
        // Implement try/catch to prevent app from crashing
        try
        {
            bookmark.setEvent(objectId);
            bookmark.setUserName(func.GetUserName());
            bookmark.setBookmark(true);
            bookmark.saveEventually();


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }   // End of setBookMark()

    private void deleteBookmark()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Bookmark");
        query.whereEqualTo("eventId", objectId);
        query.whereEqualTo("username", func.GetUserName());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    for (ParseObject delete : parseObjects) {
                        delete.deleteInBackground();
                        func.showShortToast(getApplicationContext(), "You have unmark this");
                    }

                } else {
                    func.showShortToast(getApplicationContext(), "Error occur");

            }
            }
        });
    }   // End of deleteBookmark()


    private void queryBookmark()
    {
        try
        {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Bookmark");
            query.whereEqualTo("eventId", objectId);
            query.whereEqualTo("username", func.GetUserName());
            // Debugging purpose
            //Log.e("Event ID", objectId);
            //Log.e("username", func.GetUserName());

            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if(object == null)
                    {
                        // nothing
                        likeButton.setLiked(false);


                    }
                    else
                    {
                        // something retrieve
                        likeButton.setLiked(true);
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    } // End of queryBookmark()

    private void delaySqlCheck(long millisecond) {

        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        // This was intended as due to 'delay' between
                        // Parse server update and local SQLite Check
                        // Or else it will execute before Parse side been updated
                        func.checkSqlInfo(getApplicationContext());
                    }
                }, millisecond
        );
    } // delaySqlCheck()

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap(double latitude, double longitude, String title) {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();


            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }

// create marker
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(title);

// Changing marker icon
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

// adding marker
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(16).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    } // End initilizeMap()




}
