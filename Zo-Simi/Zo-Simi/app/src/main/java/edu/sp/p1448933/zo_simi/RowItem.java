package edu.sp.p1448933.zo_simi;

import android.graphics.Bitmap;



/**
 * Created by zaiwei on 29/1/16.
 */
public class RowItem {
    private Bitmap image;
    private String objectId, eventTitle, eventDescription, eventRating, eventPrice;


    public RowItem(Bitmap image, String objectId, String eventTitle, String eventDescription, String eventRating, String eventPrice)
    {
        this.image = image;
        this.objectId = objectId;
        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;
        this.eventRating = eventRating;
        this.eventPrice = eventPrice;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventRating() {
        return eventRating;
    }

    public void setEventRating(String eventRating) {
        this.eventRating = eventRating;
    }

    public String getEventPrice() {
        return eventPrice;
    }

    public void setEventPrice(String eventPrice) {
        this.eventPrice = eventPrice;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
