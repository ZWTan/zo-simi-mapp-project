package edu.sp.p1448933.zo_simi;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

// How to scale down bitmap size/memory footprint: http://developer.android.com/training/displaying-bitmaps/load-bitmap.html


/**
 * Created by zaiwei on 20/1/16.
 * The purpose of this .java class is to recycle function
 */
public class Functionality {

    private Context context;
    private int countLikeEvent;
    private boolean isUserLogged = false;
    private String currentAuthor, url;


    private DBHelper dbHelper;


    /**
     * initializing Parse required information
     * @param context use getApplicationContext()
     */
    public void initParse(Context context)
    {
        this.context = context.getApplicationContext();

        try{
            // Disabled Parse.enableLocalDatastore to allow 'View event' function to work properly
            // Parse.enableLocalDatastore(context.getApplicationContext());
            Parse.initialize(context.getApplicationContext());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Show long toast
     * @param context use getApplicationContext()
     * @param message input the message wish to show
     */
    public void showLongToast(Context context, String message){
        this.context=context.getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    /**
     * Show short toast
     * @param context use getApplicationContext()
     * @param message input the message wish to show
     */
    public void showShortToast(Context context, String message){
        this.context=context.getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    /**
     * Return date format in dd-MM-yyyy by converting Parse full length date/time format
     * @param fullDate use to get Parse date format and reduce it formatting
     * @return date in format dd-MM-yyyy
     */
    public String getDate(Date fullDate)
    {
        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format(fullDate);
        return date;
    }


    /**
     * This functionality use for network status checking
     * @param context use getApplicationContext()
     * @return boolean of network status
     */
    public boolean CheckConnectivity(Context context)
    {
        this.context = context.getApplicationContext();
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    /**
     *
     * @return
     * status of what user's logon status
     */
    public boolean UserLogonStatus()
    {
        try
        {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if(currentUser  != null)
            {
                isUserLogged = true;
            }
            else
            {
                isUserLogged = false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return isUserLogged;
    }

    /**
     *
     * @return
     * Get current user's Parse logon name
     */
    public String GetUserName()
    {
        try{
            ParseUser currentUser = ParseUser.getCurrentUser();
            if(currentUser  != null)
            {
                currentAuthor = currentUser.getUsername();
            }
            else
            {
                currentAuthor = "";
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return currentAuthor;
    }

    /**
     *
     * @param context use getApplicationContext()
     */
    public void checkSqlInfo(Context context)
    {
        this.context = context.getApplicationContext();

        dbHelper = new DBHelper(context.getApplicationContext());
        // Sample purpose to test dummy info
        //dbHelper.insertEvent("W8UYOhpsHU", "Wah chinatown", "14", "ZWTest", "Haha", "3.0", "Food");

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery("Bookmark");
        eventQuery.whereEqualTo("username", this.GetUserName());

        try{
            eventQuery.countInBackground(new CountCallback() {
                @Override
                public void done(int count, ParseException e) {
                    countLikeEvent = count;
                    // Debugging purpose to check if SQLite DB and Parse DB Data matches
                    Log.d("ParseCountEvent", "COUNTER : " + countLikeEvent);
                    Log.d("SQLiteCounter", "COUNTER : " + dbHelper.countBookmark());
                    if(dbHelper.countBookmark() != count)
                    {
                        Log.d("populateInfoIntoSQL", "Refreshing SQLite, Removing all and populate");
                        dbHelper.deleteAllEvents();
                        populateSql();
                    }
                    else
                    {
                        // Data retained
                        Log.d("populateInfoIntoSQL", "No refresh needed");

                    }
                } // End done()
            }); // End countInBackground()
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    } // End checkSqlInfo()


    private void populateSql() {

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery("Event");
        //eventQuery.whereEqualTo("author", func.GetUserName());
        //eventQuery.orderByDescending("createdAt");

        eventQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    // OK
                    for (ParseObject results : objects) {
                        try {

                            retrieveImage(results.getObjectId(), results.getString("eventname"), results.getString("description"),
                                    results.getString("rating"), results.getString("price"),
                                    results.getString("author"), results.getString("category"),
                                    results.getString("ImageName"));
                            // Debugging purpose, to see all objectId on Parse
                            //Log.d("Check ID", results.getObjectId());

                        } catch (Exception ex) {
                            Log.d("Exception Insert DB", ex.getMessage());
                        }

                    }
                    populateBookmark();

                } else {
                    // ERROR
                    e.printStackTrace();
                }
            }
        });
    } // End populateSql()

    private void populateBookmark()
    {
        ParseQuery<ParseObject> bookmarkQuery = ParseQuery.getQuery("Bookmark");
        bookmarkQuery.whereEqualTo("username", GetUserName());
        bookmarkQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    //OK
                    for (ParseObject bookmark : objects) {
                        // Debugging purpose to view what ObjectId is being retrieved
                        //Log.e("functionality dbHelper", "insert bookmark object ID: " + bookmark.getObjectId());
                        dbHelper.insertBookmark(bookmark.getObjectId(), bookmark.getString("eventId"), bookmark.getString("username"));
                    }
                } else {
                    // ERROR
                    e.printStackTrace();
                }
            }
        });

    } // End populateBookmark()


    private void retrieveImage(final String objectIdString,final String eventName, final String description,
                               final String rating,final String price, final String author, final String category,
                               final String imageName)
    {

        // Debugging purpose to check data passed in
        // Log.e("BrowseEvent retrievedInfo", objectIdString + " " + eventName + " " + description + " " +
        //  rating + " " + price );

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        //query.orderByDescending("createdAt");
        query.getInBackground(objectIdString, new GetCallback<ParseObject>() {

            @Override
            public void done(final ParseObject object, ParseException e) {
                if (e == null) {
                    // OK
                    final ParseFile image = object.getParseFile("ImageFile");
                    {
                        image.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                url = image.getUrl();
                                // Debugging purpose
                                //Log.d("FunctionalityImgURL", url);
                                //Bitmap bitmaps = BitmapFactory.decodeByteArray(data, 0, data.length);
                                dbHelper.insertEvent(objectIdString, eventName,
                                        price, author,
                                        description, rating,
                                        category, imageName, data, url);
                            }


                        });
                    }
                }
            }
        });
    }

    /**
     * Function to reduce image file size by resizing (For upload)
     * @param file      (Path of the file)
     * @param width     (Intended width size)
     * @param height    (Intended height size)
     * @return
     */
    Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    /**
     * For retrieving to scale down it image also reducing memory footprint
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth)
        {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    } // End of calculateInSampleSize()


    public static Bitmap decodeSampledBitmapFromByte(byte[] data, int resId,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, resId, data.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, resId, data.length, options);
    }   // End of decodeSampleBitmapFromResource

    // End for retrieving image

    /**
     * Moving activity to Home.class
     * @param context use getApplicationContext()
     */
    public void GoToHome(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to BrowseEvent.class
     * @param context use getApplicationContext()
     */
    public void GoToBrowseEvent(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, BrowseEvent.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to LoginSignup.class
     * @param context use getApplicationContext()
     */
    public void GoToLogin(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, LoginSignup.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.getApplicationContext().startActivity(i);

    }

    /**
     * Moving activity to SuggestEvent.class
     * @param context use getApplicationContext()
     */
    public void GoToSuggestEvent(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, SuggestEvent.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to EmailSignUp.class
     * @param context use getApplicationContext()
     */
    public void GoToSignUp(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, EmailSignUp.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to Help.class
     * @param context use getApplicationContext()
     */
    public void GoToHelp(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, Help.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to About.class
     * @param context use getApplicationContext()
     */
    public void GoToAbout(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, About.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to FAQ.class
     * @param context use getApplicationContext()
     */
    public void GoToFAQ(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, FAQ.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to ContactUs.class
     * @param context use getApplicationContext()
     */
    public void GoToContactUs(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, ContactUs.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }


    /**
     * Moving activity to Profile.class
     * @param context use getApplicationContext()
     */
    public void GoToProfile(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, Profile.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to Like.class
     * @param context use getApplicationContext()
     */
    public void GoToLike(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, Like.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

    /**
     * Moving activity to LikeEventDetail.class
     * @param context use getApplicationContext()
     */
    public void GoToLikeDetail(Context context)
    {
        this.context = context.getApplicationContext();
        Intent i = new Intent(this.context, LikeEventDetail.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }


}
