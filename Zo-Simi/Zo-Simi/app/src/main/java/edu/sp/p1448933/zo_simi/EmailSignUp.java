package edu.sp.p1448933.zo_simi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class EmailSignUp extends AppCompatActivity {

    String usernametxt;
    Button buttonSignUp;
    EditText inputUserName, inputPassword, inputEmail;

    private Functionality func = new Functionality();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_sign_up);

        inputUserName = (EditText)findViewById(R.id.etUsername);
        inputPassword = (EditText)findViewById(R.id.etPassword);
        inputEmail = (EditText)findViewById(R.id.etEmail);
        buttonSignUp = (Button)findViewById(R.id.signUpButton);


        Bundle extra = getIntent().getExtras();
        if(extra != null)
        {
            // Collect info from previous intent (LoginSignup.class)
            usernametxt = extra.getString("Extra_username");
            inputUserName.setText(usernametxt);
        }
        else
        {
            Log.d("EmailSignUp", "no extra value from LoginSignup Activitiy");
        }

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Check device connectivity
                if(func.CheckConnectivity(getApplicationContext()) == false)
                {
                    // Debugging purpose
                    //func.showShortToast(getApplicationContext(), "Network status: " + Boolean.toString(func.CheckConnectivity(getApplicationContext())));
                    func.showShortToast(getApplicationContext(), "You're not connected to the internet");
                }
                else
                {
                    // Check if email edit text is empty or not
                    if (inputEmail.length() != 0) {
                        // Create new user account action
                        ParseUser user = new ParseUser();
                        user.setUsername(inputUserName.getText().toString());
                        user.setPassword(inputPassword.getText().toString());
                        user.setEmail(inputEmail.getText().toString());

                        user.signUpInBackground(new SignUpCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    // Let them user the app now
                                    func.showLongToast(getApplicationContext(), "Sign up successfully! Welcome to Zo-Simi!");
                                    GoToHome();
                                } else {
                                    // Sign up didn't succeed. Look at the ParseException
                                    // to figure out what went wrong
                                    if (e.getMessage().contains("already taken")) {
                                        func.showLongToast(getApplicationContext(), "Your username has already been taken. Please try again.");
                                    } else if (e.getMessage().contains("invalid email address")) {
                                        func.showLongToast(getApplicationContext(), "Please enter a valid email address");
                                    } else if (e.getMessage().contains("Password cannot be missing or blank")) {
                                        func.showLongToast(getApplicationContext(), "Please enter your password");
                                    } else if (e.getMessage().contains("email address")) {
                                        func.showLongToast(getApplicationContext(), "Your email has already been taken. Please try again.");
                                    } else if (e.getMessage().contains("Username cannot be missing or blank")) {
                                        func.showLongToast(getApplicationContext(), "Please enter user name");
                                    } else {
                                        func.showLongToast(getApplicationContext(), "Aww, error occur. Please try again");
                                    }
                                    // Debugging purpose
                                    Log.e("Sign Up Error", e.getMessage());

                                }
                            }
                        }); // End user.signUpInBackground
                    } else
                    {
                        // Checking if email edit text has info or not
                        func.showLongToast(getApplicationContext(), "Please input a valid email address.");
                    }
                }

            }   // End onClick (View view)
        }); // End buttonSignUp onClickListener
    }

    private void GoToHome()
    {
        Intent i = new Intent(this, Home.class);
        startActivity(i);
        finish();
    }

}
