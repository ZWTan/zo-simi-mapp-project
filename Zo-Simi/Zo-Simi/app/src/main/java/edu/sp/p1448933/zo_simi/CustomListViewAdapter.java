package edu.sp.p1448933.zo_simi;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import java.util.List;

/**
 * Created by zaiwei on 29/1/16.
 */

// Reference to Customview with Image and Text : http://theopentutorials.com/tutorials/android/listview/android-custom-listview-with-image-and-text-using-arrayadapter/

public class CustomListViewAdapter extends ArrayAdapter<RowItem> {
    Context context;
    ViewHolder holder;

    public CustomListViewAdapter(Context context, int resourceId,
                                 List<RowItem> items)
    {
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder
    {
        ImageView imageView;
        RatingBar ratingBarEvent;
        TextView txtTitle, txtPrice, txtDescription;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listview_item, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.eventListTitle);
            holder.txtDescription = (TextView) convertView.findViewById(R.id.eventListDescription);
            holder.txtPrice = (TextView) convertView.findViewById(R.id.eventListPrice);
            holder.imageView = (ImageView) convertView.findViewById(R.id.eventImageView);
            holder.ratingBarEvent = (RatingBar) convertView.findViewById(R.id.eventListRating);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtTitle.setText(rowItem.getEventTitle());
        holder.txtDescription.setText(rowItem.getEventDescription());
        if(rowItem.getEventPrice().startsWith("0") == false)
        {
            holder.txtPrice.setText("Estimated $" + rowItem.getEventPrice());
        }
        else
        {
            holder.txtPrice.setText("Pricing not available");
        }

        holder.ratingBarEvent.setRating(Float.parseFloat(rowItem.getEventRating()));
        holder.imageView.setImageBitmap(rowItem.getImage());


        return convertView;
    }
}
