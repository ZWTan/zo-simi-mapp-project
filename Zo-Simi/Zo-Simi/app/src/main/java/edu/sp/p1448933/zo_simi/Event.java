package edu.sp.p1448933.zo_simi;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;


/**
 * Created by zaiwei on 20/1/16.
 * Reference : http://michaelevans.org/blog/2013/08/14/tutorial-building-an-android-to-do-list-app-using-parse/
 */
@ParseClassName("Event")
public class Event extends ParseObject {
    public Event()
    {

    }

    public boolean isCompleted()
    {
        return getBoolean("completed");
    }

    public void setCompleted(boolean complete)
    {
        put("completed", complete);
    }

    public String getDescription()
    {
        return getString("description");
    }

    public void setDescription(String description)
    {
        put("description", description);
    }

    public String getCategory()
    {
        return getString("category");
    }

    public void setCategory(String category)
    {
        put("category", category);
    }

    public String getEventName()
    {
        return getString("eventname");
    }

    public void setEventName(String eventName)
    {
        put("eventname", eventName);
    }

    public String getPrice()
    {
        return getString("price");
    }

    public void setPrice(String price)
    {
        put("price", price);
    }

    public String getRating()
    {
        return getString("rating");
    }

    public void setRating(String rating)
    {
        put("rating", rating);
    }

    public String getAuthor()
    {
        return getString("author");
    }

    public String getLatitude() { return getString("latitude"); }

    public void setLatitude(String latitude) { put("latitude", latitude); }

    public String getLongitude() { return getString("longitude"); }

    public void setLongitude(String longitude) { put("longitude", longitude); }

    public String getLocationName() { return getString("locationname"); }

    public void setLocationName(String locationname) { put("locationname", locationname); }

    public void setAuthor(String author)
    {
        put("author", author);
    }

    public void setImageName(String imageName)
    {
        put("ImageName", imageName);
    }
    public String getImageName()
    {
        return getString("ImageName");
    }

    public void setImageFile(ParseFile inputFile)
    {
        put("ImageFile", inputFile);
    }

    public ParseFile getImageFile()
    {
        return getParseFile("ImageFile");
    }



}
