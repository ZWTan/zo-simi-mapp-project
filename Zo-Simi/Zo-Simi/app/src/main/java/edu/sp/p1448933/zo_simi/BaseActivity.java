package edu.sp.p1448933.zo_simi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import com.parse.ParseUser;

import java.lang.reflect.Field;

// Ref for menubar select item : http://stackoverflow.com/questions/31871586/how-to-get-menuitem-position-in-the-listener-using-the-new-navigationview
// Ref for creating NavigationBar : http://mateoj.com/2015/06/21/adding-toolbar-and-navigation-drawer-all-activities-android/

public class BaseActivity extends AppCompatActivity {

    private String screenTitle = "Activity Title";
    private NavigationView navView;
    private Functionality func = new Functionality();
    private MenuItem mi;


    private Button browseBtn, homeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void setContentView(int layoutResID)
    {

        DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        // Enable the menu button or aka hamberger icon/logo
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, fullView, toolbar,
                R.string.app_name, R.string.app_name);
        fullView.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        if (useToolbar())
        {
            setTitle(screenTitle);

        }
        else
        {
            toolbar.setVisibility(View.GONE);
        }


        browseBtn = (Button) findViewById(R.id.sliderBrowseBtn);
        homeButton = (Button) findViewById(R.id.sliderHomeBtn);



        navView = (NavigationView) findViewById(R.id.navigationView);
        Menu m = navView.getMenu();
        m.add("Profile");
        m.add("Liked");
        m.add("Help");
        m.add("Logout");
        mi = m.getItem(m.size()-1);


        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                item.setChecked(true);
                Log.e("CLICK ITEM", item.getTitle().toString());

                if(item.getTitle().toString() == "Profile")
                {
                    func.GoToProfile(getApplicationContext());
                    //Log.e("Profile", "Profile");
                }
                else if(item.getTitle().toString() == "Liked")
                {
                    func.GoToLike(getApplicationContext());
                    //func.showShortToast(getApplicationContext(), "WIP - Liked");
                    //Log.e("Liked", "Liked");
                }
                else if(item.getTitle().toString() == "Help")
                {
                    func.GoToHelp(getApplicationContext());
                }
                else if(item.getTitle().toString() == "Logout")
                {
                    confirmDialog("Log out?", "Are you sure");
                }
                //navView.getMenu().getItem(item.getItemId()).setChecked(false);
                return true;
            }
        });

        browseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(func.CheckConnectivity(getApplicationContext()) == true)
            {
                func.GoToBrowseEvent(getApplicationContext());
                //navView.getMenu().getItem(0).setChecked(false);
            }
            else
            {
                func.showShortToast(getApplicationContext(), "Please turn on your Wi-Fi or Data");
            }

            }
        });

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.GoToHome(getApplicationContext());
                //navView.getMenu().getItem(0).setChecked(false);
            }
        });


    }



    // Allowing activites to opt-out of having a toolbar
    protected boolean useToolbar()
    {
        return true;
    }


    public void setItemSelected(int position)
    {
        navView.getMenu().getItem(position).setChecked(true);
        Log.e("BaseActivity ItemID",Integer.toString(position));
    }

    public void setTitle(String inputTitle)
    {
        this.screenTitle = inputTitle;
    }

    private void confirmDialog(String title, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //BaseActivity.super.onBackPressed;
                        ParseUser.logOut();
                        func.GoToLogin(getApplicationContext());
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        navView.getMenu().getItem(3).setChecked(false);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


}
