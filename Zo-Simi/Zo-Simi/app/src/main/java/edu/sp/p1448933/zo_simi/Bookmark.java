package edu.sp.p1448933.zo_simi;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by zaiwei on 26/1/16.
 */
@ParseClassName("Bookmark")
public class Bookmark extends ParseObject {

    public Bookmark()
    {

    }

    public boolean isCompleted()
    {
        return getBoolean("completed");
    }

    public void setCompleted(boolean complete)
    {
        put("completed", complete);
    }

    public boolean isBookmark()
    {
        return getBoolean("bookmark");
    }

    public void setBookmark(boolean bookmark)
    {
        put("bookmark", bookmark);
    }

    public String getEventId ()
    {
        return getString("eventId");
    }

    public void setEvent(String eventId)
    {
        put("eventId", eventId);
    }

    public String getUserName()
    {
        return getString("username");
    }

    public void setUserName(String username)
    {
        put("username", username);
    }


}
