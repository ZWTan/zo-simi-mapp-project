package edu.sp.p1448933.zo_simi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// Reference for DB : http://www.androidauthority.com/use-sqlite-store-data-app-599743/

/**
 * Created by zaiwei on 27/1/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Zosimi.db";
    private static final int DATABASE_VERSION = 1;

    // Event table
    public static final String EVENT_TABLE_NAME = "event";
    public static final String EVENT_COLUMN_ID = "_id";
    public static final String EVENT_OBJECT_ID = "objectId";
    public static final String EVENT_COLUMN_EVENT_NAME = "eventname";
    public static final String EVENT_COLUMN_PRICE ="price";
    public static final String EVENT_COLUMN_AUTHOR = "author";
    public static final String EVENT_COLUMN_DESCRIPTION = "description";
    public static final String EVENT_COLUMN_RATING = "rating";
    public static final String EVENT_COLUMN_CATEGORY = "category";
    public static final String EVENT_COLUMN_IMAGENAME = "image_name";
    public static final String EVENT_COLUMN_IMAGEFILE ="image_data";
    public static final String EVENT_COLUMN_IMAGEURL = "image_url";

    // Bookmark table
    public static final String BOOKMARK_TABLE_NAME = "bookmark";
    public static final String BOOKMARK_COLUMN_ID = "_id";
    public static final String BOOKMARK_OBJECT_ID = "objectId";
    public static final String BOOKMARK_EVENT_ID = "eventId";
    public static final String BOOKMARK_COLUMN_USERNAME = "username";

    private int countBookmark;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // For Event Table
        db.execSQL("CREATE TABLE " + EVENT_TABLE_NAME + " (" +
                        EVENT_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        EVENT_OBJECT_ID + " TEXT UNIQUE, " +
                        EVENT_COLUMN_EVENT_NAME + " TEXT, " +
                        EVENT_COLUMN_PRICE + " TEXT, " +
                        EVENT_COLUMN_AUTHOR + " TEXT, " +
                        EVENT_COLUMN_DESCRIPTION + " TEXT, " +
                        EVENT_COLUMN_RATING + " TEXT, " +
                        EVENT_COLUMN_CATEGORY + " TEXT, " +
                        EVENT_COLUMN_IMAGENAME + " TEXT, " +
                        EVENT_COLUMN_IMAGEFILE + " BLOB, " +
                        EVENT_COLUMN_IMAGEURL + " TEXT)");

        // For Bookmark Table
        db.execSQL("CREATE TABLE " + BOOKMARK_TABLE_NAME + " (" +
                        BOOKMARK_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        BOOKMARK_OBJECT_ID + " TEXT UNIQUE, " +
                        BOOKMARK_EVENT_ID + " TEXT UNIQUE, " +
                        BOOKMARK_COLUMN_USERNAME + " TEXT)"
        );
    }   // End onCreate()

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(db);
    } // End onUpgrade()

    public boolean insertEvent(String objectId, String eventName, String price, String author, String description, String rating, String category, String imagename, byte[] imagefile, String imageurl)
    {
        // Use WritableDatabase to get SqLiteDatabase
        // object to reference to already created database
        SQLiteDatabase db = getWritableDatabase();
        try
        {
            // Event detail stored in Contentvalues object
            ContentValues contentValues = new ContentValues();
            contentValues.put(EVENT_OBJECT_ID, objectId);
            contentValues.put(EVENT_COLUMN_EVENT_NAME, eventName);
            contentValues.put(EVENT_COLUMN_PRICE, price);
            contentValues.put(EVENT_COLUMN_AUTHOR, author);
            contentValues.put(EVENT_COLUMN_DESCRIPTION, description);
            contentValues.put(EVENT_COLUMN_RATING, rating);
            contentValues.put(EVENT_COLUMN_CATEGORY, category);
            contentValues.put(EVENT_COLUMN_IMAGENAME, imagename);
            contentValues.put(EVENT_COLUMN_IMAGEFILE, imagefile);
            contentValues.put(EVENT_COLUMN_IMAGEURL, imageurl);

            db.insert(EVENT_TABLE_NAME, null, contentValues);
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
        finally
        {
            db.close();
        }
    }




    public boolean insertBookmark(String objectId, String eventId, String username)
    {

        // Use WritableDatabase to get SqLiteDatabase
        // object to reference to already created database
        SQLiteDatabase db = getWritableDatabase();
        try
        {

            // Event detail stored in Contentvalues object
            ContentValues contentValues = new ContentValues();
            contentValues.put(BOOKMARK_OBJECT_ID, objectId);
            contentValues.put(BOOKMARK_EVENT_ID, eventId);
            contentValues.put(BOOKMARK_COLUMN_USERNAME, username);

            db.insert(BOOKMARK_TABLE_NAME, null, contentValues);
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
        finally
        {
            db.close();
        }
    }

    public Cursor getEvent(String objectId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME +
                " WHERE " + EVENT_OBJECT_ID + " = '" + objectId + "' ", null);
        return res;
    }

    public Cursor getAllEvents()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME, null);
        return res;
    }

    public Cursor getLikeEvents(String userName)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME + " e, " +
                BOOKMARK_TABLE_NAME + " b" +
                " WHERE b.eventId = e.objectId " +
                " AND b.username  = '" + userName + "' ", null);
        return res;
    }

    public Cursor getPostedEvents(String userName)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME +
                " WHERE author  = '" + userName + "' ", null);
        return res;
    }

    public Cursor getSingleLikeEvent(String objectId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME + " e, " +
                BOOKMARK_TABLE_NAME + " b" +
                " WHERE b.eventId = e.objectId " +
                " AND b." + EVENT_OBJECT_ID + " = '" + objectId + "' ", null);

        return res;
    }

    public Cursor getCarouselInfo()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + EVENT_TABLE_NAME +
                " LIMIT 4 ", null);
        return res;
    }

    public Boolean updateEventName(String objectId, String eventName)
    {
        SQLiteDatabase db = getWritableDatabase();
        try
        {
            ContentValues values = new ContentValues();
            values.put("eventname", eventName);
            db.update(EVENT_TABLE_NAME, values, "objectId = ? ", new String[] { objectId });
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        finally
        {
            db.close();
        }
    }

    public Integer deleteEvent(Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(EVENT_TABLE_NAME, EVENT_COLUMN_ID + " = ? ",
                new String[] {Integer.toString(id)});
    }

    public void deleteBookmark(String objectId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(BOOKMARK_TABLE_NAME, BOOKMARK_OBJECT_ID + " = ? ", new String[] { objectId });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.close();
        }

    }

    public Integer countBookmark()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            String countQuery = "SELECT * FROM " + BOOKMARK_TABLE_NAME;
            Cursor cursor = db.rawQuery(countQuery, null);
            countBookmark = cursor.getCount();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.close();
        }

        return countBookmark;

    }

    public void deleteAllEvents()
    {
        try
        {
            getWritableDatabase().execSQL("DELETE FROM " + EVENT_TABLE_NAME + ";");
            getWritableDatabase().execSQL("DELETE FROM " + BOOKMARK_TABLE_NAME + ";");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }




}
