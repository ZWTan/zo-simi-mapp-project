package edu.sp.p1448933.zo_simi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Rachel on 5/2/2016.
 */
public class SplashScreen extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        // Debugging purpose
        //Log.e("Splashscreen", "I have started Splashscreen!");
        Intent intent = new Intent(this,LoginSignup.class);
        startActivity(intent);
        finish();

    }
}
