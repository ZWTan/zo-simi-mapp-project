package edu.sp.p1448933.zo_simi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;


import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.ByteArrayOutputStream;
import java.util.List;

// Rating Bar Ref: http://stackoverflow.com/questions/3858600/how-to-make-ratingbar-to-show-five-stars
// Spinner Ref:    http://android--examples.blogspot.sg/2015/03/spinner-in-android.html
// Upload image to parse: http://stackoverflow.com/questions/16292853/how-to-upload-an-image-in-parse-server-using-parse-api-in-android
// Storing/Retrieving file to parse: https://teamtreehouse.com/community/storing-and-retrieving-user-uploaded-images-in-parse
// SharePreferences Ref: http://stackoverflow.com/questions/15683765/activity-onpause-how-to-save-the-interface-data

//public class SuggestEvent extends AppCompatActivity {
public class SuggestEvent extends BaseActivity {
    private RatingBar ratingBar;
    private Button btnUpload, btnUploadImage;
    private EditText inputEventName, inputDescription, inputLocation;
    private String selectedCategoryText, currentAuthor, imageFileName, picturePath = "";
    private TextView lowPrice;
    private SeekBar priceBar;
    private ImageView imageViewer;
    private int selectedPrice;
    private float rating;
    private boolean spinnerChanged = false;
    private SharedPreferences sharedPreferences;
    private LatLng location;
    private double locationLong = 0.0, locationLat = 0.0;

    private Functionality func = new Functionality();
    private static final int RESULT_LOAD_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_event);
        setTitle(getResources().getString(R.string.title_addevent));

        ParseObject.registerSubclass(Event.class);

        currentAuthor = func.GetUserName();
        final Spinner spinner = (Spinner) findViewById(R.id.spinnerCategory);

        // For spinner bar
        //spinner = (Spinner) findViewById(R.id.spinnerCategory);
        // Create arrayadapter by using string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.category_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choice appear
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Now apply the adpater to spinner
        spinner.setAdapter(adapter);

        // spinner item selected listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //We are here that mean an item was selected
                //Now we retrieve the user selection
                //Get the selected item text
                if (position != 0) {
                    selectedCategoryText = parent.getItemAtPosition(position).toString();
                    spinnerChanged = true;
                } else {
                    spinnerChanged = false;
                }

                // Debug purpose, Display the toast notification on user interface for category
                // func.showShortToast(getApplicationContext(), selectedCategoryText);
                // Debug purpose, Display the toast notification on user interface for rating
                //func.showShortToast(getApplicationContext(), Float.toString(rating));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        }); // end spinner OnItemSelectedListener
        // End spinner bar

        // For rating bar
        ratingBar = (RatingBar) findViewById(R.id.eventRating);
        // End rating bar

        // For seekbar
        priceBar = (SeekBar) findViewById(R.id.seekPrice);

        priceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                lowPrice = (TextView) findViewById(R.id.tvLowPrice);
                if (seekBar == priceBar)
                    lowPrice.setText("SGD: " + progress);

                selectedPrice = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // End seekbar

        // For upload image
        btnUploadImage = (Button) findViewById(R.id.btnUploadImage);
        inputEventName = (EditText) findViewById(R.id.etEventName);
        inputDescription = (EditText) findViewById(R.id.etDescription);
        inputLocation = (EditText) findViewById(R.id.etLocation);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        imageViewer = (ImageView) findViewById(R.id.imageView);

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Simple pass Intent
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        imageViewer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Simple pass Intent
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });



        // Start for SharePreferences
        // Retrieving info when returning to app with 'killed'
        sharedPreferences = getPreferences(MODE_PRIVATE);
        String testValue = sharedPreferences.getString("eventName", "");
        // Only retrieve if user has already input 'Event name' before hand
        if(testValue != null && !testValue.equals(""))
        {
            inputEventName.setText(sharedPreferences.getString("eventName", ""));
            inputDescription.setText(sharedPreferences.getString("eventDescription", ""));
            spinner.setSelection(adapter.getPosition(sharedPreferences.getString("eventCategory", "")));
            priceBar.setProgress(Integer.parseInt(sharedPreferences.getString("eventPrice", "")));
            ratingBar.setRating(Float.parseFloat(sharedPreferences.getString("eventRating", "")));
            inputLocation.setText(sharedPreferences.getString("eventLocation", ""));
        }



        // End sharePreferences

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get ratingbar (star) rating, on 0.5 incremental
                rating = ratingBar.getRating();

                // Debugging for address
                Log.d("CHECK ADDRESS", "CHECK ADDRESS: " + inputLocation.getText().toString());
                try
                {
                    location = getLocationFromAddress(getApplicationContext(), inputLocation.getText().toString());
                    // Debugging for Lat/Long
                    locationLong = location.longitude;
                    locationLat = location.latitude;
                    Log.e("SuggestEvent", "Lat : " + Double.toString(locationLong) + "  Long : " + Double.toString(locationLat));

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    func.showShortToast(getApplicationContext(), "Error in location, please input again");
                }


                if (inputEventName.getText().length() <= 0) {
                    func.showShortToast(getApplicationContext(), "Please enter event title");
                } else if (spinnerChanged == false) {
                    func.showShortToast(getApplicationContext(), "Please select a category");
                } else if (rating <= 0) {
                    func.showShortToast(getApplicationContext(), "Please rate this event");
                } else if (inputDescription.getText().length() <= 0) {
                    func.showShortToast(getApplicationContext(), "Please enter description");
                } else if (picturePath.isEmpty() == true) {
                    func.showShortToast(getApplicationContext(), "Please select an image");
                }
                else if(inputLocation.getText().length() <= 0) {
                    func.showShortToast(getApplicationContext(), "Please provide location");
                }

                if (inputEventName.getText().length() > 0 && inputDescription.getText().length() > 0
                        && spinnerChanged != false && rating > 0 && picturePath.length() > 0 &&
                        inputLocation.length() > 0 && locationLat > 0.1) {

                    Event event = new Event();
                    event.setEventName(inputEventName.getText().toString());
                    event.setCategory(selectedCategoryText);
                    event.setPrice(Integer.toString(selectedPrice));
                    event.setDescription(inputDescription.getText().toString());
                    event.setRating(Float.toString(rating));
                    event.setAuthor(currentAuthor);
                    // For Google MAP
                    event.setLatitude(Double.toString(location.latitude));
                    event.setLongitude(Double.toString(location.longitude));
                    event.setLocationName(inputLocation.getText().toString());
                    event.setCompleted(false);

                    if (picturePath != null) {
                        // Setting up image upload
                        try {
                            Bitmap bitmap;
                            // Locate the image
                            // Only format jpg
                            if (picturePath.contains(".jpg") || picturePath.contains(".jpeg")) {
                                bitmap = func.ShrinkBitmap(picturePath, 300, 300);
                            } else {
                                bitmap = BitmapFactory.decodeFile(picturePath);
                            }

                            // resize image
                            // Convert it to byte
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            // Compress image to lower quality scale 1 - 100
                            // Parse only accept ~10MB
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);


                            byte[] image = stream.toByteArray();

                            // Create the ParseFile
                            ParseFile file = new ParseFile(imageFileName, image);
                            // Upload the image into Parse Cloud
                            file.saveInBackground();

                            // Image Name
                            event.setImageName(imageFileName);

                            // Insert ImageFile
                            event.setImageFile(file);
                            // create it
                            event.saveInBackground();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // saveEventually() is use to queue object to be saved. Incase when user doesn't have Internet access
                    event.saveEventually();
                    event.setEventName("");
                    event.setCategory("");
                    event.setPrice("");
                    event.setRating("");
                    event.setDescription("");
                    event.setAuthor("");
                    event.setLongitude("");
                    event.setLatitude("");
                    event.setLocationName("");
                    // Clear all sharedPrefInfo after successfully submitted
                    clearInfo(spinner);
                    spinner.setSelection(0);
                    //event.setImageFile(null);
                    //event.setImageName("");
                    func.showShortToast(getApplicationContext(), "Thank you for suggesting this!");
                    // After finish pushing
                    func.GoToHome(getApplicationContext());
                }

            }
        }); // end btnUpload OnClickListener

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            picturePath = cursor.getString(columnIndex);
            cursor.close();

            // Get Image filename
            if (picturePath.length() > 0) {
                imageFileName = picturePath.substring(picturePath.lastIndexOf("/") + 1, picturePath.length());
            }

            //ImageView imageView = (ImageView) findViewById(R.id.imageView);
            imageViewer.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            imageViewer.setImageURI(selectedImage
            );

        }


    } // End of onActivityResult()

    @Override
    protected void onPause() {
        super.onPause();
        // During onPause, start saving required info into SharedPreferences
        storeSharedPrefInfo();
    } // End onPause()


    private void storeSharedPrefInfo() {
        //   Share preferences info
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        // Clear before saving new
        editor.clear();
        editor.putString("eventName", inputEventName.getText().toString());
        editor.putString("eventDescription", inputDescription.getText().toString());
        editor.putString("eventCategory", selectedCategoryText);
        editor.putString("eventPrice", Integer.toString(selectedPrice));
        editor.putString("eventRating", Float.toString(ratingBar.getRating()));
        editor.putString("eventLocation", inputLocation.getText().toString());

        editor.commit();
    }   // End storeSharedPrefInfo

    private void clearInfo(Spinner spinner)
    {
        inputEventName.setText("");
        inputDescription.setText("");
        //int spinnerIndex = getSpinnerIndex(spinner, sharedPreferences.getString("eventCategory",""));
        spinner.setSelection(0);
        priceBar.setProgress(0);
        ratingBar.setRating(0);
    }   // End clearInfo()


    private int getSpinnerIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if(spinner.getSelectedItem() == myString)
            {
                index = i;
            }
        }
        return index;
    }   // End getSpinnerIndex()

    public LatLng getLocationFromAddress(Context context,String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // To force geocoder to search within Singapore
            address = coder.getFromLocationName(strAddress + ", Singapore, Singapore", 1);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}


