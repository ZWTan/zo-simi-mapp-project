package edu.sp.p1448933.zo_simi;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;


// Ref for storing image in SQLite and retrieving: http://stackoverflow.com/questions/11790104/how-to-storebitmap-image-and-retrieve-image-from-sqlite-database-in-android
// Ref for populating data to ListView from SQLite: http://androidsolution4u.blogspot.sg/2013/09/android-populate-listview-from-sqlite.html
// Ref potential 'SwipeToDelete' plugin: https://github.com/daimajia/AndroidSwipeLayout

//public class Like extends AppCompatActivity {
public class Like extends BaseActivity{

    public final static String KEY_EXTRA_EVENT_ID = "KEY_EXTRA_CONTACT_ID";


    private ListView listView;
    private DBHelper dbHelper;
    private Functionality func = new Functionality();

    private RowItem item;
    private List<RowItem> rowItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like);
        // Highlight the navigation drawer as 1 to represent 2nd in item, start from 0
        setItemSelected(1);
        setTitle(getResources().getString(R.string.title_like));
        dbHelper = new DBHelper(this);
        rowItems = new ArrayList<RowItem>();
        listView = (ListView) findViewById(R.id.listviewLike);

        /* Populate SQLite data onto ListView! */

        rowItems.clear();
        Cursor cursor = dbHelper.getLikeEvents(func.GetUserName());

        //Log.d("Like CursorCount", Integer.toString(cursor.getCount()));


        if(cursor.moveToFirst())
        {
            try
            {
                do {
                    item = new RowItem(func.decodeSampledBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_IMAGEFILE)), 0, 120, 120), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_OBJECT_ID)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_EVENT_NAME)),
                            cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_DESCRIPTION)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_RATING)), cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_PRICE)));
                    rowItems.add(item);
                    Log.d("Like CountEvent", Integer.toString(rowItems.size()));
                } while (cursor.moveToNext());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


        }

        CustomListViewAdapter adapter = new CustomListViewAdapter(Like.this, R.layout.listview_item, rowItems);
        adapter.notifyDataSetChanged();

        // Binds the Adapter to the ListView
        listView.setAdapter(adapter);

        cursor.close();

        /*
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Debugging purpose
                //func.showShortToast(getApplicationContext(), "YOU CLICKED ON ME : " + position
                //      + " rowitem pos: " + rowItems.get(position).getObjectId());

                // Send single item click data to SingleItemView Class
                Intent i = new Intent(Like.this,
                        LikeEventDetail.class);
                // Pass data "name" followed by the position
                // Place the data here ("variable name" from intent) and getString("variable name") from Parse column name
                i.putExtra("objectId", rowItems.get(position).getObjectId());
                Log.d("LikeScreen", "Like Screen Obj Id: " + rowItems.get(position).getObjectId());
                // Open BrowseEventDetail.java Activity
                startActivity(i);
            }
        }); // End setOnItemClickListener()




        if(dbHelper.countBookmark() <= 0)
        {
            func.showShortToast(getApplicationContext(), "No Liked Event");
        }

    } // End onCreate


}
