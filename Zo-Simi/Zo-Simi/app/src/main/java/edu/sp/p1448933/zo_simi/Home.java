package edu.sp.p1448933.zo_simi;


import android.database.Cursor;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.HashMap;

// Building Navigation Drawer Ref: http://blog.teamtreehouse.com/add-navigation-drawer-android
// Ref for Homepage slider carousel plugin: https://github.com/daimajia/AndroidImageSlider

//public class Home extends AppCompatActivity {
public class Home extends BaseActivity
{
    private Button suggestBtn, eventListBtn;
    private SliderLayout mDemoSlider;
    private TextSliderView textSliderView;
    private DBHelper dbHelper;
    private HashMap<String,String> url_maps;

    private Functionality func = new Functionality();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setTitle(getResources().getString(R.string.title_home));
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        // Populating current user's 'LIKE' list into SQLite
        // Only check if theres connectivity
        if(func.CheckConnectivity(getApplicationContext()) == true)
        {
            func.checkSqlInfo(getApplicationContext());
        }
        else
        {
            Log.d("CheckSqlFail", "No Connectivity");
        }

        // End of populating!

        suggestBtn = (Button) findViewById(R.id.suggestBtn);
        eventListBtn = (Button) findViewById(R.id.browseBtn);
        setupCarousel();


        suggestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (func.UserLogonStatus() == false) {
                    func.showShortToast(getApplicationContext(), "Please login to enjoy this feature");
                } else {
                    func.GoToSuggestEvent(getApplicationContext());
                }

            }
        });

        eventListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(func.CheckConnectivity(getApplicationContext()) == true)
                {
                    func.GoToBrowseEvent(getApplicationContext());
                }
                else
                {
                    func.showShortToast(getApplicationContext(), "Please turn on your Wi-Fi or Data");
                }
            }
        });

    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on
        // the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }


    private void setupCarousel()
    {

        dbHelper = new DBHelper(this);

        //if(dbHelper.getAllEvents().getCount() > 0) {
            // Debugging purpose
            //Log.e("Count Top4: ", Integer.toString(dbHelper.getCarouselInfo().getCount()));
            try {
                Cursor cursor = dbHelper.getCarouselInfo();
                cursor.moveToFirst();

                url_maps = new HashMap<String, String>();

                int eventNameCol = cursor.getColumnIndex(dbHelper.EVENT_COLUMN_EVENT_NAME);
                int eventImageUrlCol = cursor.getColumnIndex(dbHelper.EVENT_COLUMN_IMAGEURL);
                do {
                    // Create object of bitmapfactory's option method for further option use
                    //BitmapFactory.Options options = new BitmapFactory.Options();
                    // inPurgeable is used to free up memory while required
                    //options.inPurgeable = true;
                    //bitmaps = BitmapFactory.decodeByteArray(cursor.getBlob(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_IMAGEFILE)), 0, cursor.getBlob(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_IMAGEFILE)).length, options);
                    //bitmaps = BitmapFactory.decodeByteArray(cursor.getBlob(9), 0, cursor.getBlob(9).length, options);

                    //url_maps.put(cursor.getString(2), cursor.getString(10));

                    url_maps.put(cursor.getString(eventNameCol), cursor.getString(eventImageUrlCol));
                }
                while (cursor.moveToNext());

            } catch (Exception ex)
            {
                ex.printStackTrace();
            }

            // sample of how online data like
            //url_maps.put("Wah chinatown", "http://files.parsetfss.com/7507084e-feb0-402c-8069-b037a8f204f0/tfss-8c9d8424-5fb9-4f39-9bab-2307d50737f5-IMG_20160126_184648.jpg");
            //url_maps.put("Event 1", "http://files.parsetfss.com/7507084e-feb0-402c-8069-b037a8f204f0/tfss-ed431c92-889d-47df-b813-87b69da1193b-IMG_20160116_171228.jpg");
            //url_maps.put("Parse is shutting down", "http://files.parsetfss.com/7507084e-feb0-402c-8069-b037a8f204f0/tfss-e0b9c0ea-860e-458e-94e8-3b07374af307-Screenshot_20160129-070841.png");


            // Offline version
            HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
            file_maps.put("Hannibal", R.drawable.test);
            file_maps.put("Big Bang Theory", R.drawable.test1);
            file_maps.put("House of Cards", R.drawable.test2);
            file_maps.put("Game of Thrones", R.drawable.test3);
            // Check if there's internet to grab info from and SQLite has any record
            // If have, than use the online information, or else use static info as temporary
            if (func.CheckConnectivity(getApplicationContext()) == true && dbHelper.getAllEvents().getCount() > 0) {
                for (String name : url_maps.keySet()) {
                    textSliderView = new TextSliderView(this);
                    // initialize a SliderLayout
                    textSliderView
                            .description(name)
                            .image(url_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit);

                    //add your extra information
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }
            }
            else
            {
                for (String name : file_maps.keySet()) {
                    textSliderView = new TextSliderView(this);
                    // initialize a SliderLayout
                    textSliderView
                            .description(name)
                            .image(file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit);

                    //add your extra information
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }

            }


            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
            // 6 Second per flip
            mDemoSlider.setDuration(6000);

    } // End SetupCarousel()

}
