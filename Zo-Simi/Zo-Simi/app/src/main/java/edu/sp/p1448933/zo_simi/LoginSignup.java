package edu.sp.p1448933.zo_simi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginSignup extends AppCompatActivity {

    String usernametxt, passwordtxt;
    EditText inputUserName, inputPassword;
    Button loginButton, signupButton;

    private Functionality func = new Functionality();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);

        inputUserName = (EditText) findViewById(R.id.etUsername);
        inputPassword = (EditText) findViewById(R.id.etPassword);
        loginButton = (Button) findViewById(R.id.loginBtn);
        signupButton = (Button) findViewById(R.id.signupBtn);

        // [Optional] Power your app with Local Datastore. For more info, go to
        // https://parse.com/docs/android/guide#local-datastore
        // CALL THIS ONCES ONLY

        func.initParse(getApplicationContext());

        // If user already logged in, Directly log-in
        if (func.UserLogonStatus() == true) {
            func.GoToHome(getApplicationContext());
            finish();
        }


        // Login Button Click Listener
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Check device connectivity
                if (func.CheckConnectivity(getApplicationContext()) == false) {
                    // Debugging purpose
                    //func.showShortToast(getApplicationContext(), "Network status: " + Boolean.toString(func.CheckConnectivity(getApplicationContext())));
                    func.showShortToast(getApplicationContext(), "You're not connected to the internet");
                } else {
                    // Perform action on click
                    // func.showShortToast(getApplicationContext(), "I HAVE CLICKED ON YOU!");
                    // Retrieving the text from EditText
                    usernametxt = inputUserName.getText().toString();
                    passwordtxt = inputPassword.getText().toString();
                    if (usernametxt.length() <= 0 || passwordtxt.length() <= 0) {
                        // empty on edittext
                        func.showLongToast(getApplicationContext(), "Please filled up for login");
                    } else {
                        // Send data to Parse.com for verification
                        ParseUser.logInInBackground(usernametxt, passwordtxt,
                                new LogInCallback() {
                                    @Override
                                    public void done(ParseUser user, ParseException e) {
                                        if (user != null) {
                                            // If user exist and authentic
                                            func.GoToHome(getApplicationContext());
                                            func.showShortToast(getApplicationContext(), "Successfully logged in");
                                            finish();
                                        } else {
                                            // Debug purpose
                                            //Log.d("Login issue", e.getMessage().toString());
                                            func.showLongToast(getApplicationContext(), "Incorrect username or password");
                                        }
                                    }
                                });
                    }
                }
            }
        }); // End loginButton OnClickListener

        // Sign-up Button Click Listener
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Retrieve text from EditText
                usernametxt = inputUserName.getText().toString();
                passwordtxt = inputPassword.getText().toString();

                Intent i = new Intent(LoginSignup.this, EmailSignUp.class);
                // Pass user intended user name to signup page to carry on signing up process
                i.putExtra("Extra_username", usernametxt);
                startActivity(i);
                //finish();
            }
        });
    }


}

