package edu.sp.p1448933.zo_simi;

import android.os.Bundle;

public class FAQ extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        setTitle(getResources().getString(R.string.title_faq));
    }
}
