package edu.sp.p1448933.zo_simi;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.like.IconType;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class LikeEventDetail extends BaseActivity {

    private TextView eventLikeTitle, eventLikeDescription, eventLikePrice, eventLikeAuthor, eventLikeCategory, eventLikePostDate;
    private RatingBar eventLikeDetailRating;
    private String objectId;
    private ImageView eventLikeImage;
    private Functionality func = new Functionality();
    private com.like.LikeButton likeButton;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_event_detail);
        dbHelper = new DBHelper(this);

        setTitle(getResources().getString(R.string.title_like_detail));

        eventLikeTitle = (TextView)findViewById(R.id.tvLikeEventTitle);
        eventLikeDescription = (TextView)findViewById(R.id.tvLikeEventDescription);
        eventLikeDetailRating = (RatingBar)findViewById(R.id.eventLikeDetailRating);
        eventLikePrice = (TextView)findViewById(R.id.tvLikeEventPrice);
        eventLikeAuthor = (TextView)findViewById(R.id.tvLikeEventAuthor);
        eventLikeCategory = (TextView)findViewById(R.id.tvLikeEventCategory);
        //eventLikePostDate = (TextView)findViewById(R.id.tvLikeEventPostDate);
        eventLikeImage = (ImageView) findViewById(R.id.ivLikeEventImage);
        likeButton = (com.like.LikeButton) findViewById(R.id.eventLike_button);
        // Set the likebutton to thumb icon
        likeButton.setIcon(IconType.Thumb);
        eventLikeDetailRating.setFocusable(false);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            // Collect info from previous intent (BrowseEvent.class)
            objectId = extra.getString("objectId");
        }
        // Debugging purpose to see the objectId pass it over from
        // previous activity
        //func.showShortToast(getApplicationContext(), "LikeEventDetailScreen Obj ID: " + objectId);

        populateInfo(objectId);


        likeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeLike(objectId);
            }
        });

    }


    private void populateInfo(String objectId)
    {
        // Double check if input is a valid or not
        if(objectId != null && objectId.isEmpty() == false)
        {
            Cursor cursor = dbHelper.getSingleLikeEvent(objectId);
            cursor.moveToFirst();
            // Debugging purpose
            Log.d("LikeEventDetail Count", "Number of event on ID: " + objectId + " / " + Integer.toString(cursor.getCount()));

            eventLikeTitle.setText(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_EVENT_NAME)));
            eventLikeDescription.setText(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_DESCRIPTION)));
            eventLikeDetailRating.setRating(Float.parseFloat(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_RATING))));
            if(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_PRICE)).startsWith("0") == false)
            {
                eventLikePrice.setText("Estimated $" + cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_PRICE)));
            }
            else
            {
                eventLikePrice.setText("Pricing not available");
            }


            eventLikeAuthor.setText(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_AUTHOR)));
            eventLikeCategory.setText(cursor.getString(cursor.getColumnIndex(dbHelper.EVENT_COLUMN_CATEGORY)));
            //eventLikePostDate.setText(cursor.getString(cursor.getColumnIndex(dbHelper.Event)));

            eventLikeImage.setImageBitmap(func.decodeSampledBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(DBHelper.EVENT_COLUMN_IMAGEFILE)), 0, 300, 300));


            if(cursor.getCount() <= 0)
            {
                likeButton.setLiked(false);
            }
            else
            {
                likeButton.setLiked(true);
            }

        } else {

            func.showShortToast(getApplicationContext(), "Sorry some error have occur");
        }

    }   // End of populateInfo()

    private void removeLike(String objectId)
    {
        // Remove in SQLite
        dbHelper.deleteBookmark(objectId);
        func.showShortToast(getApplicationContext(), "Successfully remove from Like");
        // Remove in Parse

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Bookmark");
        query.whereEqualTo("objectId", objectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null)
                {
                    try{
                        for(ParseObject find : objects)
                        {
                            find.deleteEventually();
                            func.GoToLike(getApplicationContext());
                            finish();
                        }

                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }

                }
            }

        });
    }   // End removeLike()

}
