package edu.sp.p1448933.zo_simi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;
import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseException;
import com.parse.ParseQuery;

// Example of using Parse LocalDatabase: https://www.parse.com/tutorials/using-the-local-datastore
// Dealing with Async status: http://stackoverflow.com/questions/10042922/check-for-working-asynctask

public class BrowseEvent extends BaseActivity {

    private Functionality func = new Functionality();

    // Declare variable
    private ListView listview;
    private List<ParseObject> ob;
    private ProgressDialog mProgressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int countEvent;
    private RowItem item;
    private List<RowItem> rowItems;
    private FloatingActionButton fabSuggest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_event);


        setTitle(getResources().getString(R.string.title_browseEvent));
        fabSuggest = (FloatingActionButton) findViewById(R.id.fabSuggest);
        fabSuggest.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        fabSuggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.GoToSuggestEvent(getApplicationContext());
            }
        });

        // Execute RemoteDataTask AsyncTask
        rowItems = new ArrayList<RowItem>();
        new RemoteDataTask().execute();

        // Configure the swipe refresh layout
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Start showing the refreshing animation
                mSwipeRefreshLayout.setRefreshing(true);


                // Simulate a long running activity
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RemoteDataTask rdt = new RemoteDataTask();
                        rdt.execute();

                        // If ASync done, stop spinning
                        if (rdt.getStatus() == AsyncTask.Status.RUNNING) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }, 0);
            }
        });

    }   // End onCreate



    // RemoteDataTask AsyncTask
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(BrowseEvent.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Fetching information");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        } // End onPreExecute()



        @Override
        protected Void doInBackground(Void... params) {
            // Create the array

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "Event");
            // Although Suggest already checked for image
            // This is a second check in case somehow, somewhat bug in system
            query.whereExists("ImageFile");
            query.orderByDescending("createdAt");
            try {
                ob = query.find();
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        } // End doInBackground()


        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.lvEvent);
            // Retrieve object "name" from Parse.com database
            rowItems.clear();
            try
            {
                for (ParseObject event : ob) {
                        // Pushing information into another method
                        retrieveInfo(event.getObjectId(), event.getString("eventname"),
                                event.getString("description"), event.getString("rating"), event.getString("price"));
                    }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        } // End of onPostExecute()
    } // End of RemoteDataTask()

    private void retrieveInfo (final String objectIdString,final String eventName, final String description,
                                final String rating,final String price)
    {
        // Debugging purpose to check data passed in
        // Log.e("BrowseEvent retrievedInfo", objectIdString + " " + eventName + " " + description + " " +
        //  rating + " " + price );

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        //query.orderByDescending("createdAt");
        query.getInBackground(objectIdString, new GetCallback<ParseObject>() {

            @Override
            public void done(final ParseObject object, ParseException e) {
                if (e == null) {
                    // OK
                    ParseFile image = object.getParseFile("ImageFile");
                    {
                        image.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                item = new RowItem(func.decodeSampledBitmapFromByte(data, 0, 120, 120),
                                        objectIdString, eventName,
                                        description, rating, price);

                            rowItems.add(item);
                            // Get number of events
                            countEvent = rowItems.size();
                            // Debug purpose
                            //Log.d("BrowseEvent COUNT ITEM", Integer.toString(countEvent));

                            if(countEvent == 0)
                            {
                                // If no event retrieved
                                func.showShortToast(getApplicationContext(), "Oops! sorry no event yet");
                                //Log.e("BrowseEvent No Event", Integer.toString(countEvent));
                            }
                            else
                            {
                                // Debugging purpose
                                //Log.e("BrowseEvent Have Event", "Number of event: " + Integer.toString(countEvent));
                            }

                            CustomListViewAdapter adapter = new CustomListViewAdapter(BrowseEvent.this,R.layout.listview_item, rowItems);

                            //Notifies the attached observers that the underlying data has been
                            // changed and any View reflecting the data set should refresh itself.
                            adapter.notifyDataSetChanged();

                            // Binds the Adapter to the ListView
                            listview.setAdapter(adapter);

                            // Close the progressdialog
                            mProgressDialog.dismiss();

                            // Capture button clicks on ListView items
                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    // Send single item click data to SingleItemView Class
                                    Intent i = new Intent(BrowseEvent.this,
                                            BrowseEventDetail.class);
                                    // Pass data "name" followed by the position
                                    // Place the data here ("variable name" from intent) and getString("variable name") from Parse column name
                                    i.putExtra("objectId", rowItems.get(position).getObjectId());
                                    // Open BrowseEventDetail.java Activity
                                    startActivity(i);
                                }
                            }); // end setOnItemClickListener
                            }
                        });
                    }
                }
                else
                {
                    // PROBLEM fetching event info
                    e.printStackTrace();
                }

            } // End done()

        }); // End of query.getDataInBackground()
    }   // End of retrieveImage()




}