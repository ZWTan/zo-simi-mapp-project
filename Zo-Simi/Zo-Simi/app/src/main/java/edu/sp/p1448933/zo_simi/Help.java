package edu.sp.p1448933.zo_simi;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//public class Help extends AppCompatActivity {
public class Help extends BaseActivity  {

    private Functionality func = new Functionality();

    private Button aboutButton, faqButton, contactusButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setTitle(getResources().getString(R.string.title_help));
        setItemSelected(2);

        aboutButton = (Button) findViewById(R.id.aboutBtn);
        faqButton = (Button) findViewById(R.id.faqBtn);
        contactusButton = (Button) findViewById(R.id.contactusBtn);


        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.GoToAbout(getApplicationContext());
            }
        });

        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.GoToFAQ(getApplicationContext());

            }
        });

        contactusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.GoToContactUs(getApplicationContext());

            }
        });

    }
}
