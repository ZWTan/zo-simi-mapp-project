package edu.sp.p1448933.zo_simi;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ContactUs extends BaseActivity {

    private Button btnSend;
    private Functionality func;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        func = new Functionality();
        setTitle(getResources().getString(R.string.title_contactus));

        btnSend = (Button)findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                func.showShortToast(getApplicationContext(), "Submitted sucessfully");
            }
        });

    }
}
